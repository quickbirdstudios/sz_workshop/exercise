//
//  LoginViewController.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 08.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class LoginViewController: UIViewController {
    // TODO: Add view model

    @IBOutlet private var emailField: UITextField!
    @IBOutlet private var passwordField: UITextField!
    @IBOutlet private var loginButton: UIButton!

    // TODO: Add disposeBag

    override func viewDidLoad() {
        super.viewDidLoad()

        // TODO: Add navigation to employees view (inside a navigation controller)

        // TODO: Add input bindings for email and password

        // TODO: Add output bindings for the login button enabled state
    }
}
