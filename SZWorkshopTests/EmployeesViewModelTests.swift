//
//  SZWorkshopTests.swift
//  SZWorkshopTests
//
//  Created by Stefan Kofler on 07.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import XCTest
import RxBlocking
import RxSwift

class EmployeesViewModelTests: XCTestCase {

    private var viewModel: EmployeesViewModel!

    override func setUp() {
        super.setUp()

        viewModel = EmployeesViewModelImpl()
    }

    func testEmployeesFilter() {
        let employeesObservable = viewModel.output.employees.asObservable()

        viewModel.input.filterText.onNext("St")

        let employees = try! employeesObservable.toBlocking().first()
        XCTAssertEqual(employees, ["Stefan"])
    }

}
